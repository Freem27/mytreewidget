#ifndef MYTREEWIDGET_H
#define MYTREEWIDGET_H

#include "QTreeWidget"
#include "QTreeWidgetItem"

class MyTreeWidget : public QTreeWidget
{
public:
    int itemCount();
    void filter(QString str);
    explicit MyTreeWidget(QWidget *parent = 0);
    void acceptDrop();
    void rejectDrop();

protected:
    virtual void dropEvent(QDropEvent *event);
    virtual void mousePressEvent(QMouseEvent *event);
    virtual void dragMoveEvent(QDragMoveEvent *event);

signals:
    void dropItem(QTreeWidgetItem *from,QTreeWidgetItem *to,QTreeWidgetItem *item);

};

#endif // MYTREEWIDGET_H
