#-------------------------------------------------
#
# Project created by QtCreator 2016-06-02T10:16:26
#
#-------------------------------------------------

QT       += widgets

TARGET = MyTreeWidget
TEMPLATE = lib

#DEFINES += MYTREEWIDGET_LIBRARY

DESTDIR = dist
SOURCES += mytreewidget.cpp

HEADERS += mytreewidget.h

VERSION = 1.0.0

TARGET = $$qtLibraryTarget(MyTreeWidget)

CONFIG += build_all
unix {
    target.path = /usr/lib
    INSTALLS += target
}
