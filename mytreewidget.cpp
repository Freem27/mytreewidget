#include "mytreewidget.h"

int MyTreeWidget::itemCount()
{
    int cnt=topLevelItemCount();
    for(int i=0;i<topLevelItemCount();i++)
        cnt=cnt+getChildCount(topLevelItem(i));
    return cnt;
}

void MyTreeWidget::setFilter(QString str,QList<int> columns)
{
    collapseAll();
    if(str.simplified()=="")
    {
        for(int i=0;i<topLevelItemCount();i++)
        {

            setItemColor(topLevelItem(i),QColor(255,255,255));
            applyFilter(topLevelItem(i),"1564845135484546155132454897654",columns); //допилить нормальное обнуление цвета итема
        }
        return;
    }
    for(int i=0;i<topLevelItemCount();i++)
    {
        bool contain=false;
        for(int c=0;c<columns.count();c++)
            if(topLevelItem(i)->text(columns.at(c)).toUpper().contains(str.toUpper()))
            {
                contain=true;
                break;
            }

        if(contain)
            setItemColor(topLevelItem(i),QColor(212,175,55));
        else
            setItemColor(topLevelItem(i),QColor(255,255,255));
        applyFilter(topLevelItem(i),str,columns);
    }
}

void MyTreeWidget::applyFilter(QTreeWidgetItem *par,QString str,QList<int> columns)
{
    for(int i=0;i<par->childCount();i++)
    {
        bool contain=false;
        for(int c=0;c<columns.count();c++)
            if(par->child(i)->text(columns.at(c)).toUpper().contains(str.toUpper()))
            {
                contain=true;
                break;
            }


        if(contain)
        {
            setItemColor(par->child(i),QColor(212,175,55));
            par->child(i)->setExpanded(true);
            QTreeWidgetItem *exp=par;
            while(exp)
            {
                exp->setExpanded(true);
                exp=exp->parent();
            }
        }
        else
            setItemColor(par->child(i),QColor(255,255,255));
        applyFilter(par->child(i),str,columns);
    }
}

void MyTreeWidget::setItemColor(QTreeWidgetItem *it,QColor col)
{
    if(!it)
        return;
    for(int i=0;i<it->columnCount();i++)
        it->setBackground(i,QBrush(col));
}

MyTreeWidget::MyTreeWidget(QWidget *parent) :
QTreeWidget(parent)
{
    QSizePolicy sizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);
    sizePolicy.setHorizontalStretch(0);
    sizePolicy.setVerticalStretch(0);
    //sizePolicy.setHeightForWidth(this->sizePolicy().hasHeightForWidth());
   // setSizePolicy(sizePolicy);

    setAcceptDrops(true);
    setDragEnabled(true);
    setDragDropMode(QAbstractItemView::InternalMove);
}

void MyTreeWidget::acceptDrop()
{
    if(currEvent)
        QTreeWidget::dropEvent(currEvent);

}

void MyTreeWidget::rejectDrop()
{
    currEvent->ignore();
}

QList<QTreeWidgetItem *> MyTreeWidget::items()
{
    QList<QTreeWidgetItem *> result;
    for(int i=0;i<topLevelItemCount();i++)
    {
        result.append(topLevelItem(i));
        result.append(getChildItems(topLevelItem(i)));
    }
    return result;
}

QList<QTreeWidgetItem *> MyTreeWidget::getChildItems(QTreeWidgetItem *item)
{
    QList<QTreeWidgetItem *> result;
    for(int i=0;i<item->childCount();i++)
    {
        result.append(item->child(i));
        result.append(getChildItems(item->child(i)));
    }

    return result;
}

void MyTreeWidget::expandToTop(QTreeWidgetItem *item)
{
    QTreeWidgetItem *parent;
    if(item->parent())
        parent=item->parent();
    while(parent)
    {
        expandItem(parent);
        if(parent->parent())
            parent=parent->parent();
        else
            break;
    }
}


void MyTreeWidget::mousePressEvent(QMouseEvent *event)
{
    if (event->button() == Qt::LeftButton and this->itemAt(event->pos()))
    {
        dragItem=this->itemAt(event->pos());
    }
    QTreeWidget::mousePressEvent(event);
}

void MyTreeWidget::dropEvent(QDropEvent *event)
{
    QString from=QString::null;
    dragItem->parent() ? from=dragItem->parent()->text(0) : from;
    QString into=QString::null;
    itemAt(event->pos()) ? into=itemAt(event->pos())->text(0):into;

    if(from!=into)
    {
        QTreeWidgetItem *itemTo=itemAt(event->pos());
        QTreeWidgetItem *itemParent=dragItem->parent();
        currEvent=event;
        emit dropItem(itemParent,itemTo,dragItem);
        emit dropItem(itemTo,dragItem);
    }

}

void MyTreeWidget::dragMoveEvent(QDragMoveEvent *event)
{
    if((itemAt(event->pos()) and bool(itemAt(event->pos())->flags() & Qt::ItemIsDropEnabled)) or !itemAt(event->pos()))
        QTreeWidget::dragMoveEvent(event);
    else
        event->setAccepted(false);
}

int MyTreeWidget::getChildCount(QTreeWidgetItem *item)
{
    int cnt=item->childCount();
    for(int i=0;i<item->childCount();i++)
        cnt=cnt+getChildCount(item->child(i));
    return cnt;

}
