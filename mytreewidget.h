#ifndef MYTREEWIDGET_H
#define MYTREEWIDGET_H

#include <QObject>
#include "QTreeWidget"
#include <QDropEvent>
#include "QDragEnterEvent"
#include "QTreeWidgetItem"
#include <QMessageBox>

class MyTreeWidget : public QTreeWidget
{
    Q_OBJECT
public:
    int itemCount();
    void setFilter(QString str, QList<int> columns=(QList<int>()<<0));
    explicit MyTreeWidget(QWidget *parent = 0);
    void acceptDrop();
    void rejectDrop();
    QList<QTreeWidgetItem*> items();

    QList<QTreeWidgetItem *> getChildItems(QTreeWidgetItem *item);
    void expandToTop(QTreeWidgetItem *item);
    void setItemColor(QTreeWidgetItem *it, QColor col);

protected:
    virtual void dropEvent(QDropEvent *event);
    virtual void mousePressEvent(QMouseEvent *event);
    virtual void dragMoveEvent(QDragMoveEvent *event);

signals:
    void dropItem(QTreeWidgetItem *from,QTreeWidgetItem *to,QTreeWidgetItem *item);
    void dropItem(QTreeWidgetItem *to,QTreeWidgetItem *item);

private:
    int getChildCount(QTreeWidgetItem *item);
    void applyFilter(QTreeWidgetItem *par, QString str, QList<int> columns);
    QTreeWidgetItem *dragItem;
    QDropEvent *currEvent;
};

#endif // MYTREEWIDGET_H
